# Sworp Admin Dashboard

## Getting started

To make it easy for you to get started with installation and running dashboard, here's a list of recommended next steps.

## Setup the project

Clone the project in the desired directory
```
git clone https://gitlab.com/purnima392/sworp.git
cd sworp
npm install
```

This will install all the necessary dependencies like tailwind css, flowbite, chart js and leaflet js.

## Run the project

Run the project using the command
```
npm run dev
```
This command will use Vite to build and serve the project. Vite is a fast development server and build tool for modern web applications.

Once the server is running, you can access the dashboard application in your web browser by visiting http://localhost:5173
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    '**/*.html',
    "./node_modules/flowbite/**/*.js"
  ],
  theme: {
    extend: {
      colors: {
        'primary': {
          light: '#78D804',
          DEFAULT: '#66BA00',
          dark: '#539603',
          opacity: '#F0F9E6',
        }, 
        'light-gray': {
          light: '#F9F9F9',
          DEFAULT: '#F7F7F7',
          dark: '#EBEBEB',
        },      
      } 
    },
    container: {
      padding: {
        DEFAULT: '1rem',
        sm: '2rem',
        lg: '5rem',
        xl: '6rem',
        '2xl': '10rem',
      },
    },           
  },
  plugins: [
    require('flowbite/plugin')
  ],
}


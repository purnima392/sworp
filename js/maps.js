var map = L.map("map").setView([53.468, -2.2359], 13);
L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
  maxZoom: 17,
}).addTo(map);
// var customIcon = L.icon({
//   iconUrl: '/images/svg/ion_water.svg',
//   iconSize: [32, 32],
//   iconAnchor: [16, 16],
// });
var marker1 = L.marker([53.468, -2.2359]).addTo(map);
var popupContent1 = `
  <div class="max-w-md">
      <img class="max-w-xs" style="width:20px;" src="/images/svg/house.svg" />
      <p class="text-md text-black">Property Name</p> 
      <a href="#">
          View More
      </a>
  </div>
  `;
marker1.bindPopup(popupContent1);

var marker2 = L.marker([53.457, -2.2769]).addTo(map);
var popupContent2 = `
  <div class="max-w-md">
  <div class="flex gap-x-2">
  <div class="icon"><img class="max-w-xs" style="width:20px;" src="/images/svg/house.svg" /></div>
  <div>
    <p class="text-xs text-black m-0">Property Name Two</p>
    <a href="#">
        View More
    </a>
  </div>
  </div>
      
     
  </div>
  `;
marker2.bindPopup(popupContent2);

var marker3 = L.marker([53.471, -2.178]).addTo(map);
var popupContent3 = `
  <div class="max-w-sm">
      <img class="max-w-xs" style="width:20px;" src="/images/svg/house.svg" />
      <p class="text-md text-black">Property Name Three</p> 
      <a href="#">
          View More
      </a>
  </div>
  `;
marker3.bindPopup(popupContent3);

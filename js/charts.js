var propertyRevenue = document
  .getElementById("property-revenue-chart")
  .getContext("2d");

var barChartData = {
  labels: [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
  ],
  datasets: [
    {
      label: "Income",
      data: [10, 15, 12, 16, 11, 22, 18, 6, 19, 3],
      backgroundColor: "#F8752B",
      borderColor: "#F8752B",
      borderWidth: 1,
      borderRadius: 5,
    },
    {
      label: "Expenses",
      data: [15, 5, 18, 16, 21, 12, 8, 16, 11, 9],
      backgroundColor: "#66BA00",
      borderColor: "#66BA00",
      borderWidth: 1,
      borderRadius: 5,
    },
  ],
};

var barChartOptions = {
  responsive: true,
  barThickness: 13,
  scales: {
    x: {
      grid: {
        display: false,
      },
    },
    y: {
      grid: {
        color: "#CCCCCC",
      },
      border: {
        dash: [2, 4],
      },
      ticks: {
        callback: function (value) {
          if (value === 0) {
            return "$" + value;
          }
          return "$" + value + "k";
        },       
      },
      beginAtZero: true,
    },
  },
  plugins: {
    legend: {
      position: "bottom",
      align: "center",
      labels: {
        usePointStyle: true,
        boxWidth: 6,
        boxHeight: 6,
        
      },
    },
  },
};

var propertyRevenueChart = new Chart(propertyRevenue, {
  type: "bar",
  data: barChartData,
  options: barChartOptions,
});

var expiringLease = document
  .getElementById("expiring-lease-chart")
  .getContext("2d");

var doughnutChartData = {
  labels: ["<30 days", "31-60 days", "61-90 days"],
  datasets: [
    {
      label: "My Dataset",
      data: [10, 20, 30],
      backgroundColor: ["#FF545D", "#FF8528", "#555CB7"],
      borderColor: ["#FF545D", "#FF8528", "#555CB7"],
      borderWidth: 0,
    },
  ],
};

var doughnutChartOptions = {
  responsive: true,
  cutout: "85%",
  plugins: {
    legend: {
      position: "bottom",
      align: "center",
      labels: {
        usePointStyle: true,
        boxWidth: 6,
        boxHeight: 6,
      },
    }
  },  
};
var drawExpiringLeaseChart = new Chart(expiringLease, {
  type: "doughnut",
  data: doughnutChartData,
  options: doughnutChartOptions,
});
